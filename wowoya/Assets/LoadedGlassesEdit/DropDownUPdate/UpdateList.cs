﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateList : MonoBehaviour {
    public Dropdown LoadTest, PublicTexture, PrivateTexture;
    GameObject[] AssetE;
    Texture2D[] AssetPT, AssetPT2;
    string[] privateTextnames;
    private string updatename;    
    int DropdownNum, DropdownNum01, DropdownNum02, AssetENum=0, AssetPTNum=0, AssetPT2Num = 0;
    TextAsset namelist;
    DownloadScript getpointer;
    int selfpointer = 0, selfcount;    
    
	// Use this for initialization
	void Start () {        
        DropdownNum =LoadTest.options.Count;//下拉式選單的元素個數
        DropdownNum01 = PublicTexture.options.Count;
        getpointer = GetComponent<DownloadScript>();
        StartCoroutine(loadAsset());
        
    }
	
	// Update is called once per frame
	void Update () {
	}
    public void LoadTestClick()
    {
        PrivateTexture.options.Clear();
        Dropdown.OptionData defult = new Dropdown.OptionData("特定材質");
        PrivateTexture.options.Add(defult);
        for (int i=0;i < AssetPT2Num; i++)
        {
            string[] assetobjs = privateTextnames[i].Split('-');            
            if (LoadTest.transform.Find("Label").GetComponent<Text>().text == assetobjs[0]) {
                Dropdown.OptionData List = new Dropdown.OptionData(privateTextnames[i]);
                PrivateTexture.options.Add(List);
            }
        }
    }
    public void PrivateTextureChange()
    {
        if(GameObject.Find("ARObjects").transform.Find(LoadTest.transform.Find("Label").GetComponent<Text>().text+ "(Clone)") != null)
        {
            GameObject target = GameObject.Find("ARObjects").transform.Find(LoadTest.transform.Find("Label").GetComponent<Text>().text + "(Clone)").gameObject;
            if (target.transform.childCount == 0)
            {
                for (int i = 0; i < AssetPT2Num; i++)
                {
                    if (PrivateTexture.transform.Find("Label").GetComponent<Text>().text == AssetPT2[i].name)
                    {
                        target.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", AssetPT2[i]);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < AssetPT2Num; i++)
                {
                    if (PrivateTexture.transform.Find("Label").GetComponent<Text>().text == AssetPT2[i].name)
                    {
                        target.transform.Find(LoadTest.transform.Find("Label").GetComponent<Text>().text).GetComponent<MeshRenderer>().material.SetTexture("_MainTex", AssetPT2[i]);
                        break;
                    }
                }
            }
            
        }
    }
    public void PublicTextureChange()
    {
        if (GameObject.Find("ARObjects").transform.Find(LoadTest.transform.Find("Label").GetComponent<Text>().text + "(Clone)") != null)
        {
            GameObject target = GameObject.Find("ARObjects").transform.Find(LoadTest.transform.Find("Label").GetComponent<Text>().text + "(Clone)").gameObject;
            if (target.transform.childCount == 0)
            {
                for (int i = 0; i < AssetPTNum; i++)
                {
                    if (PublicTexture.transform.Find("Label").GetComponent<Text>().text == AssetPT[i].name)
                    {
                        target.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", AssetPT[i]);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < AssetPTNum; i++)
                {
                    if (PublicTexture.transform.Find("Label").GetComponent<Text>().text == AssetPT[i].name)
                    {
                        target.transform.Find(LoadTest.transform.Find("Label").GetComponent<Text>().text).GetComponent<MeshRenderer>().material.SetTexture("_MainTex", AssetPT[i]);
                        break;
                    }
                }
            }
        }
    }
    public void UpdateMainlist()
    {
        if(selfcount+2< AssetE.Length)
        {
            selfcount += 2;
            do
            {
                Dropdown.OptionData List = new Dropdown.OptionData(AssetE[selfpointer].name);
                LoadTest.options.Add(List);
                selfpointer++;
            } while (selfpointer < selfcount);
        }
        else
        {
            selfcount = AssetE.Length;
            do
            {
                Dropdown.OptionData List = new Dropdown.OptionData(AssetE[selfpointer].name);
                LoadTest.options.Add(List);
                selfpointer++;
            } while (selfpointer < selfcount);
        }
        
    }
    IEnumerator loadAsset()
    {

        //WWW www = new WWW("file://" + Application.dataPath + "/AssetBundleTest/AssetBundleTest");
        WWW www = getpointer.www;
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {

            Debug.Log("www load success!");
            AssetBundleManifest _ABM = www.assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            if (_ABM != null)
            {
                string[] _Assetnames = _ABM.GetAllAssetBundles();
                for (int nIndex = 0; nIndex < _Assetnames.Length; ++nIndex)
                {
                    Debug.Log("Get Asset Bundle Names List" + " :"+"("+ nIndex+")"+_Assetnames[nIndex]);
                }
                //WWW ww = new WWW("file://" + Application.dataPath + "/AssetBundleTest/" + _Assetnames[0]);
                WWW ww = getpointer.ww;
                WWW ww02 = new WWW("http://128.199.214.9/wowoya/" + _Assetnames[1]);
                WWW ww01 = new WWW("http://128.199.214.9/wowoya/" + _Assetnames[2]);
                yield return ww;
                if (string.IsNullOrEmpty(ww.error))
                {


                    AssetE = getpointer.obj;
                    AssetENum = AssetE.Length;
                    selfcount = getpointer.listcount;

                    if (DropdownNum!= AssetENum)
                    {
                        Debug.Log("New models discovered! Adding Elements........");

                        do
                        {
                            Dropdown.OptionData List = new Dropdown.OptionData(AssetE[selfpointer].name);
                            LoadTest.options.Add(List);
                            selfpointer++;
                        } while (selfpointer < getpointer.listcount);                        
                                                
                    }//如果載入項目數量多於一個的話需要加入for迴圈

                    //ww.assetBundle.Unload(false);
                }
                else
                {
                    Debug.Log("Asset" + _Assetnames[0] + "is not exist!");
                }
                yield return ww01;
                if (string.IsNullOrEmpty(ww01.error))
                {
                                       

                    AssetPT = ww01.assetBundle.LoadAllAssets<Texture2D>();
                    AssetPTNum = AssetPT.Length;

                    if (DropdownNum01 != AssetPTNum)
                    {
                        Debug.Log("New Public Texture discovered! Adding Elements.......");
                        for (int i = 0; i < AssetPTNum; i++)
                        {                           
                                Dropdown.OptionData List = new Dropdown.OptionData(AssetPT[i].name);
                                PublicTexture.options.Add(List);                           
                            
                        }

                    }//如果載入項目數量多於一個的話需要加入for迴圈

                    ww01.assetBundle.Unload(false);
                }
                else
                {
                    Debug.Log("Asset" + _Assetnames[1] + "is not exist!");
                }
                yield return ww02;
                if (string.IsNullOrEmpty(ww02.error))
                {

                    
                    AssetPT2 = ww02.assetBundle.LoadAllAssets<Texture2D>();
                    AssetPT2Num = AssetPT2.Length;                    
                    privateTextnames = new string[AssetPT2Num];
                    for(int i=0;i< AssetPT2Num; i++)
                    {                        
                        privateTextnames[i] = AssetPT2[i].name;
                    }                  
                   
                    ww02.assetBundle.Unload(false);
                }
                else
                {
                    Debug.Log("Asset" + _Assetnames[2] + "is not exist!");
                }
            }
            else
            {
                Debug.Log("No Manifest");
            }
            //www.assetBundle.Unload(false);
        }
        else
        {
            Debug.Log(www.error);
        }

    }

}
