﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GlassesAlpha : MonoBehaviour {
    float Alphavalue;
    Slider SlideBar;
	// Use this for initialization
	void Start () {
        GetComponent<MeshRenderer>().material = Resources.Load<Material>("Glasses");
        SlideBar = GameObject.Find("Alphaadjust").GetComponent<Slider>();
    }
	
	// Update is called once per frame
	void Update () {
        Alphavalue = SlideBar.value;
        GetComponent<MeshRenderer>().material.SetColor("_TintColor", new Color(1, 1, 1, Alphavalue));
    }
}
