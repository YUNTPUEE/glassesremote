﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ChildUI : MonoBehaviour {
    Transform[] Children, exceptTran;
    int ChildNum, exceptNum;
    bool switchON;
    //transition============================
    float PX = 0;
    float PY = 85;
    float PZ = -76;
    float SX = 1.05f;
    float SY = 1.05f;
    float SZ = 1.2f;
    float RX = 0;
    float RY = 180;
    float RZ = 0;
    //========================================
    GameObject[] Child;
    GameObject Dropdown, exceptOBJ, SuperTemp, AROBJ;


    string Currentname;

    // Use this for initialization
    void Start () {
        Dropdown = GameObject.Find("Canvas/Loading Test/Label");
        Text testing = Dropdown.GetComponent<Text>();
        Currentname = testing.text;

        Children = GetComponentsInChildren<Transform>();
        ChildNum = transform.childCount;
        SuperTemp = GameObject.Find("GlassesNotUse");
        AROBJ = GameObject.Find("ARObjects");
        switchON = false;
        //匯入時的transform參數       


        Child = new GameObject[ChildNum+1];

        if (ChildNum == 0)
        {
            transform.localPosition = new Vector3(PX, PY, PZ);
            transform.localScale = new Vector3(SX, SY, SZ);
            transform.localRotation = Quaternion.Euler(RX, RY, RZ);
        }
        else
        {
            transform.localPosition = new Vector3(0, 3, 22);            
            transform.localRotation = Quaternion.Euler(-6f, RY, RZ);
            transform.localScale = new Vector3(2.46f, 2.42f, 3.0f);
            Destroy(transform.Find("male").gameObject);
            if (transform.Find("Glasses") != null)
            {
                Child[0] = transform.Find("Glasses").gameObject;                
                Child[1] = transform.Find(Currentname).gameObject;
                for (int i = 0; i < 2; i++)
                {
                    Child[i].layer = 5;
                }
            }
            else
            {
                Child[0] = transform.Find(Currentname).gameObject;
                Child[0].layer = 5;
            }
        }
        

    }

    // Update is called once per frame
    void Update () {
        Dropdown = GameObject.Find("Canvas/Loading Test/Label");
        Text testing = Dropdown.GetComponent<Text>();
        Currentname = testing.text;
        
        if (gameObject.name!= Currentname + "(Clone)")
        {
            gameObject.layer=0;
            

            gameObject.transform.position = SuperTemp.transform.position;
            gameObject.transform.parent = SuperTemp.transform;
            switchON = true;
            gameObject.SetActive(false);
        }//當前眼鏡不選擇此物件
        else
        {
            if (switchON)
            {                
                switchON = false;
            }          
            
            

        }



    }
}
