﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddDestroy : MonoBehaviour {
    public GameObject copyGameObject;//要被複製的物件
    public GameObject superGameObject;//要被放置在哪個物件底下

    private GameObject childGameObject;//被複製出來的物件

    private void OnGUI()
    {
        if (GUILayout.Button("動態產生物件") == true)
        {
            childGameObject = Instantiate(copyGameObject);//複製copyGameObject物件(連同該物件身上的腳本一起複製)
            childGameObject.transform.parent = superGameObject.transform;//放到superGameObject物件內
            childGameObject.transform.localPosition = Vector3.zero;//複製出來的物件放置的座標為superGameObject物件內的原點
            //childGameObject.AddComponent<GlassProperty>();//動態增加名為"NullScript"的腳本到此物件身上

            //下面這一行的功能為將複製出來的子物件命名為CopyObject

            //childGameObject.name = "CopyObject";



            //產生一個Object的物件，並指定子物件上要被刪除的腳本
            //Object script = childGameObject.GetComponent<DeleteScript>();
            //Destroy(script);//刪除腳本
        }
        if (GUILayout.Button("動態移除物件") == true)
        {
            Destroy(childGameObject);//刪除複製出來的子物件
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
