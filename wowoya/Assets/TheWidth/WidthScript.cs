﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;
using DlibFaceLandmarkDetector;

namespace DlibFaceLandmarkDetectorSample
{
    [RequireComponent(typeof(WebCamTextureToMatHelper))]
    public class WidthScript : MonoBehaviour
    {
        public Camera ARCamera;
        MatOfDouble distCoeffs;
        Texture2D texture;        
        MatOfPoint2f imagePoints;
        
        Mat camMatrix;
        WebCamTextureToMatHelper webCamTextureToMatHelper;
        FaceLandmarkDetector faceLandmarkDetector;
        float[] headwidth = new float[5];
        int wkount = 0;
        float minW=0;
        /// <summary>
        /// The shape_predictor_68_face_landmarks_dat_filepath.
        /// </summary>
        private string shape_predictor_68_face_landmarks_dat_filepath;
        private UnityEngine.Rect WindowPosition;
        private UnityEngine.Rect ButtonPosition;
        private UnityEngine.Rect LabelPosition;
        private bool showwindow;
        // Use this for initialization
        void Start()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            StartCoroutine(DlibFaceLandmarkDetector.Utils.getFilePathAsync("shape_predictor_68_face_landmarks.dat", (result) => {
                shape_predictor_68_face_landmarks_dat_filepath = result;
                Run ();
            }));
#else
            shape_predictor_68_face_landmarks_dat_filepath = DlibFaceLandmarkDetector.Utils.getFilePath("shape_predictor_68_face_landmarks.dat");
            Run();
#endif
        }


        private void Run()
        {
            //set 3d face object points.

            imagePoints = new MatOfPoint2f();            

            faceLandmarkDetector = new FaceLandmarkDetector(shape_predictor_68_face_landmarks_dat_filepath);

            webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();
            //webCamTextureToMatHelper.Init();
        }
        /// <summary>
        /// Raises the web cam texture to mat helper inited event.
        /// </summary>
        public void OnWebCamTextureToMatHelperInited()
        {
            Debug.Log("OnWebCamTextureToMatHelperInited");

            Mat webCamTextureMat = webCamTextureToMatHelper.GetMat();

            texture = new Texture2D(webCamTextureMat.cols(), webCamTextureMat.rows(), TextureFormat.RGBA32, false);

            gameObject.GetComponent<Renderer>().material.mainTexture = texture;

            gameObject.transform.localScale = new Vector3(webCamTextureMat.cols(), webCamTextureMat.rows(), 1);
            Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);


            float width = webCamTextureMat.width();
            float height = webCamTextureMat.height();

            float imageSizeScale = 1.0f;
            float widthScale = (float)Screen.width / width;
            float heightScale = (float)Screen.height / height;
            if (widthScale < heightScale)
            {
                Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
                imageSizeScale = (float)Screen.height / (float)Screen.width;
            }
            else
            {
                Camera.main.orthographicSize = height / 2;
            }


            //set cameraparam
            int max_d = (int)Mathf.Max(width, height);
            double fx = max_d;
            double fy = max_d;
            double cx = width / 2.0f;
            double cy = height / 2.0f;
            camMatrix = new Mat(3, 3, CvType.CV_64FC1);
            camMatrix.put(0, 0, fx);
            camMatrix.put(0, 1, 0);
            camMatrix.put(0, 2, cx);
            camMatrix.put(1, 0, 0);
            camMatrix.put(1, 1, fy);
            camMatrix.put(1, 2, cy);
            camMatrix.put(2, 0, 0);
            camMatrix.put(2, 1, 0);
            camMatrix.put(2, 2, 1.0f);
            Debug.Log("camMatrix " + camMatrix.dump());


            distCoeffs = new MatOfDouble(0, 0, 0, 0);
            Debug.Log("distCoeffs " + distCoeffs.dump());


            //calibration camera
            Size imageSize = new Size(width * imageSizeScale, height * imageSizeScale);
            double apertureWidth = 0;
            double apertureHeight = 0;
            double[] fovx = new double[1];
            double[] fovy = new double[1];
            double[] focalLength = new double[1];
            Point principalPoint = new Point(0, 0);
            double[] aspectratio = new double[1];

            Calib3d.calibrationMatrixValues(camMatrix, imageSize, apertureWidth, apertureHeight, fovx, fovy, focalLength, principalPoint, aspectratio);

            Debug.Log("imageSize " + imageSize.ToString());
            Debug.Log("apertureWidth " + apertureWidth);
            Debug.Log("apertureHeight " + apertureHeight);
            Debug.Log("fovx " + fovx[0]);
            Debug.Log("fovy " + fovy[0]);
            Debug.Log("focalLength " + focalLength[0]);
            Debug.Log("principalPoint " + principalPoint.ToString());
            Debug.Log("aspectratio " + aspectratio[0]);


            //To convert the difference of the FOV value of the OpenCV and Unity. 
            double fovXScale = (2.0 * Mathf.Atan((float)(imageSize.width / (2.0 * fx)))) / (Mathf.Atan2((float)cx, (float)fx) + Mathf.Atan2((float)(imageSize.width - cx), (float)fx));
            double fovYScale = (2.0 * Mathf.Atan((float)(imageSize.height / (2.0 * fy)))) / (Mathf.Atan2((float)cy, (float)fy) + Mathf.Atan2((float)(imageSize.height - cy), (float)fy));

            Debug.Log("fovXScale " + fovXScale);
            Debug.Log("fovYScale " + fovYScale);


            //Adjust Unity Camera FOV https://github.com/opencv/opencv/commit/8ed1945ccd52501f5ab22bdec6aa1f91f1e2cfd4
            if (widthScale < heightScale)
            {
                ARCamera.fieldOfView = (float)(fovx[0] * fovXScale);
            }
            else
            {
                ARCamera.fieldOfView = (float)(fovy[0] * fovYScale);
            }
            
        }
        /// <summary>
        /// Raises the web cam texture to mat helper disposed event.
        /// </summary>
        public void OnWebCamTextureToMatHelperDisposed()
        {
            Debug.Log("OnWebCamTextureToMatHelperDisposed");

            camMatrix.Dispose();
            distCoeffs.Dispose();
        }
        // Update is called once per frame
        void Update()
        {
            if (webCamTextureToMatHelper.isPlaying() && webCamTextureToMatHelper.didUpdateThisFrame())
            {

                Mat rgbaMat = webCamTextureToMatHelper.GetMat();
                Mat rgb2gray = new Mat();
                Mat edge = new Mat();
                Mat blurdst = new Mat();
                Mat hierarchy = new Mat();
                Mat detectPlace = new Mat();

                OpenCVForUnityUtils.SetImage(faceLandmarkDetector, rgbaMat);

                //detect face rects
                List<UnityEngine.Rect> detectResult = faceLandmarkDetector.Detect();
                
                //================邊緣或輪廓===================================             
                
                List<MatOfPoint> Contours = new List<MatOfPoint>();                
                
                                
                Mat edgedst = rgbaMat.clone();
                Mat lines = new Mat();                
                
                
                //=======================臉部頂點========================================
                if (detectResult.Count > 0)
                {

                    //detect landmark points
                    List<Vector2> landmarks = faceLandmarkDetector.DetectLandmark(detectResult[0]);
                    if (landmarks.Count > 0)
                    {
                        OpenCVForUnityUtils.DrawFaceLandmark(edgedst, landmarks, new Scalar(0, 255, 0, 255), 2);
                    }

                    OpenCVForUnity.Rect rect = new OpenCVForUnity.Rect((int)landmarks[8].x-((int)landmarks[26].x - (int)landmarks[17].x)/2, (int)landmarks[5].y, (int)landmarks[26].x- (int)landmarks[17].x, (int)landmarks[57].y- (int)landmarks[30].y);//int x, int y, int width, int height
                    Imgproc.rectangle(edgedst, rect.tl(), rect.br(), new OpenCVForUnity.Scalar(255, 255, 255, 0));
                    if (rect.br().x < rgbaMat.width() && rect.tl().x>0 && rect.br().y< rgbaMat.height() && rect.tl().y >0)
                    {
                        detectPlace = rgbaMat.submat((int)rect.tl().y, (int)rect.br().y, (int)rect.tl().x, (int)rect.br().x).clone();//int rowstart, int row end, int colstart, int colend, tl()返回左上角点坐标，br()返回右下角点坐标
                        Imgproc.cvtColor(detectPlace, rgb2gray, Imgproc.COLOR_RGB2GRAY);
                        Imgproc.Canny(rgb2gray, edge, 50, 150);
                        Imgproc.findContours(edge, Contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
                        Imgproc.HoughLinesP(edge, lines, 1, Mathf.PI / 45, 20);
                        for (int i = 1; i < lines.rows(); i++)
                        {

                            double[] vec = lines.get(i, 0);
                            double x1 = vec[0],
                            y1 = vec[1],
                            x2 = vec[2],
                            y2 = vec[3];
                            Point start = new Point(x1, y1);
                            Point end = new Point(x2, y2);
                            double dx = x1 - x2;
                            double dy = y1 - y2;
                            float angle = Mathf.Atan2((float)dy, (float)dx);
                            float dist = Mathf.Sqrt((float)dx * (float)dx + (float)dy * (float)dy);
                            if (dist > ((int)landmarks[64].x - (int)landmarks[60].x) && dist < ((int)landmarks[13].x - (int)landmarks[3].x))
                            {
                                headwidth[wkount] = (landmarks[16].x - landmarks[0].x) / dist;//Imgproc.line(edgedst, start, end, new Scalar(0, 0, 255, 255), 2);
                                Debug.Log("width[" + wkount + "]=" + headwidth[wkount]);
                                wkount++;
                            }


                        }
                    }
                    else detectPlace.release();



                }

                //呈現
                if (wkount >= 4)
                {
                    for(int i = 0; i < wkount; i++)
                    {
                        if (i == 0) minW = headwidth[i];
                        if (minW > headwidth[i]) minW = headwidth[i];
                    }
                    setWindowPosition();
                    wkount = 0;
                }
                Imgproc.putText(rgbaMat, "W:" + rgbaMat.width() + " H:" + rgbaMat.height() + " SO:" + Screen.orientation, new Point(5, rgbaMat.rows() - 10), Core.FONT_HERSHEY_SIMPLEX, 0.5, new Scalar(255, 255, 255, 255), 1, Imgproc.LINE_AA, false);
                //文字
                OpenCVForUnity.Utils.matToTexture2D(edgedst, texture, webCamTextureToMatHelper.GetBufferColors());//圖
            }
        
        }
        void OnDisable ()
        {
            if (webCamTextureToMatHelper != null)
                webCamTextureToMatHelper.Dispose ();

            if (faceLandmarkDetector != null)
                faceLandmarkDetector.Dispose ();
        }
    private void setWindowPosition()
        {
            float WinWidth = 200f;
            float WinHeight = 200f;
            float windowleft = Screen.width * 0.5f - WinWidth * 0.5f;
            float windowTop= Screen.height * 0.5f - WinHeight * 0.5f;
            WindowPosition = new UnityEngine.Rect(windowleft, windowTop, WinWidth, WinHeight);
            setButtonPosition();
            setLabelPosition();
            showwindow = true;
        }
    private void setButtonPosition()
        {
            float ButWidth = 50f;
            float ButHeight = 30f;
            float Buttonleft = WindowPosition.width * 0.5f - ButWidth * 0.5f;
            float ButtonTop = WindowPosition.height * 0.7f - ButHeight * 0.5f;
            ButtonPosition = new UnityEngine.Rect(Buttonleft, ButtonTop, ButWidth, ButHeight);
        }
    private void setLabelPosition()
        {
            float LabelWidth = 80f;
            float LabelHeight = 50f;
            float Labelleft = WindowPosition.width * 0.5f - LabelWidth * 0.5f;
            float LabelTop = WindowPosition.height * 0.4f - LabelHeight * 0.5f;
            LabelPosition = new UnityEngine.Rect(Labelleft, LabelTop, LabelWidth, LabelHeight);
        }
        private void OnGUI()
        {
            if (showwindow)
            {
                GUI.Window(0, WindowPosition, windowevent, "頭部寬度");                
            }             
            
        }
    private void windowevent(int ID)
        {
            GUI.Label(LabelPosition, "頭部寬度約:" + 6.4 * minW + "cm");
            if (GUI.Button(ButtonPosition, "OK") && ID==0)
            {
                showwindow = false;
            }
        }
    }
}
