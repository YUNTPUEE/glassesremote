﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitObjects : MonoBehaviour {
    Transform[] ChildOBJs;
    int ChildOBJsNum;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ChildOBJs = GetComponentsInChildren<Transform>();
        ChildOBJsNum = transform.childCount;

        if (ChildOBJsNum >= 20)
        {
            GameObject.Destroy(ChildOBJs[1]);
        }

    }
}
