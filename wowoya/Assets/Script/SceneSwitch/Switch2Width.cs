﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Switch2Width : MonoBehaviour {
    public void SwitchScene()
    {
        GameObject.Find("Quad").SetActive(false);
        GameObject.Find("Canvas").SetActive(false);
        GameObject.Find("QuadW").SetActive(true);
        GameObject.Find("CanvasW").SetActive(true);
    }
            
    
}
