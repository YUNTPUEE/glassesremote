﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

// : MonoBehaviour 

public class AssetBundleBuilder
{

    // Use this for initialization
    //void Start () {

    //}

    // Update is called once per frame
    //void Update () {

    //}
    [MenuItem("Assets/Build AssetBundles/Android")]
    static void BuildAllAssetBundles()
    {
        Caching.CleanCache();
        int modelbuilds = 7;//以後增加模型，改這邊即可
        AssetBundleBuild[] mapbuild = new AssetBundleBuild[3];       

        mapbuild[0].assetBundleName = "models";        
        string[] assets = new string[modelbuilds];        
        assets[0] = "Assets/3DObject/wowoya/wowoya001.3DS";
        assets[1] = "Assets/3DObject/wowoya/wowoya002.3DS";
        assets[2] = "Assets/3DObject/wowoya/wowoya003.3DS";
        assets[3] = "Assets/3DObject/wowoya/wowoya004.3DS";
        assets[4] = "Assets/3DObject/wowoya/wowoya005.3DS";
        assets[5] = "Assets/3DObject/wowoya/wowoya006.3DS";
        assets[6] = "Assets/3DObject/wowoya/wowoya007.3DS";
        //副檔名"一定要加"        
        mapbuild[0].assetNames = assets;

        mapbuild[1].assetBundleName = "Public texture";
        string[] assetsPublicmat = new string[3];
        assetsPublicmat[0] = "Assets/3DObject/wowoya/public-1.png";
        assetsPublicmat[1] = "Assets/3DObject/wowoya/public-2.png";
        assetsPublicmat[2] = "Assets/3DObject/wowoya/public-3.jpg";        
        mapbuild[1].assetNames = assetsPublicmat;

        mapbuild[2].assetBundleName = "Private texture";
        string[] assetsPrivatemat = new string[4];        
        assetsPrivatemat[0] = "Assets/3DObject/wowoya/wowoya001-1.png";
        assetsPrivatemat[1] = "Assets/3DObject/wowoya/wowoya005-1.png";
        assetsPrivatemat[2] = "Assets/3DObject/wowoya/wowoya006-1.png";
        assetsPrivatemat[3] = "Assets/3DObject/wowoya/wowoya006-2.png";
        mapbuild[2].assetNames = assetsPrivatemat;

        BuildPipeline.BuildAssetBundles("Assets/AssetBundleTest", mapbuild, BuildAssetBundleOptions.None, BuildTarget.Android);
    }
}
