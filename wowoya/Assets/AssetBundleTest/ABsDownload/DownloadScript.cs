﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadScript : MonoBehaviour {
    /*public bool caches = false;
    private string address = "https://172.16.31.193/ABsUpload/AssetBundleTest";

    // Use this for initialization
    private void OnGUI()
    {
        if (GUILayout.Button("Load"))
        {
            StartCoroutine(Load(address));
        }
    }

    IEnumerator Load(string url)
    {
        Debug.Log(url);
        WWW www = null;
        if (!caches)
        {
            //一般下載
            www = new WWW(url);

        }
        else
        {
            //下載至快取
            www = WWW.LoadFromCacheOrDownload(url, 0);
        }
        yield return www;
        GameObject prefab = www.assetBundle.LoadAsset("testing01.forunity") as GameObject;
        GameObject obj = GameObject.Instantiate(prefab);
        www.Dispose();
    }*/
    
    public GameObject superGameObject;//要被放置在哪個物件底下
    public Button LoadB;
    public int listcount = 0;
    int loadpointer = 0;
    public WWW www, ww;
    GameObject target;
    string Objnames;
    GameObject GlassesTest, IFExistOBJ;
    GameObject[] GlassesChild;
    public GameObject[] obj;
    void Start () {
        LoadB.onClick.AddListener(EnableLoad);
        listcount = 3;//預載三個模型
        StartCoroutine(loadAsset());
    }
    // Update is called once per frame
    
    void Update () {

        

    }
    
    IEnumerator loadAsset()
    {
        
        //WWW www = new WWW("file://" + Application.dataPath + "/3DObject/20170407testing/20170407testing");
         www = new WWW("http://128.199.214.9/wowoya/AssetBundleTest");
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Load Success!");
            AssetBundleManifest _ABM = www.assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            if (_ABM != null)
            {
                string[] _Assetnames = _ABM.GetAllAssetBundles();
                
                //WWW ww = new WWW("file://" + Application.dataPath + "/3DObject/20170407testing" + _Assetnames[0]);
                ww = new WWW("http://128.199.214.9/wowoya/" + _Assetnames[0]);                
                yield return ww;                
                if (string.IsNullOrEmpty(ww.error))
                {

                    obj = ww.assetBundle.LoadAllAssets<GameObject>();
                    
                    do
                    {
                        if (obj[loadpointer] != null)
                        {

                            GlassesTest = Instantiate(obj[loadpointer]);
                            GlassesTest.transform.parent = GameObject.Find("GlassesNotUse").transform;//放到superGameObject物件內
                            int ChildCount = GlassesTest.transform.childCount;
                            GlassesTest.layer = 5;//層級設成UI                     


                            if (GlassesTest.transform.childCount != 0)
                            {
                                GameObject GlassT = new GameObject();

                                GlassT = GlassesTest.transform.Find(Objnames).gameObject;

                                if (GlassesTest.transform.Find("Glasses") != null)
                                {
                                    GlassT = GlassesTest.transform.Find("Glasses").gameObject;
                                    GlassT.AddComponent<GlassesAlpha>();
                                }
                            }

                            GlassesTest.AddComponent<ChildUI>();
                            GlassesTest.AddComponent<TouchDetect>();
                            GlassesTest.SetActive(false);
                        }
                        else
                        {
                            Debug.Log("No Glasses!");
                        }
                        loadpointer++;
                    } while (loadpointer < listcount);
                   

                    ww.assetBundle.Unload(false);
                   
                }
                else
                {
                    Debug.Log("Asset" + _Assetnames[0] + "is not exist!");
                }
            }
            else
            {
                Debug.Log("No Manifest");
            }

            www.assetBundle.Unload(false);
        }
        else
        {
            Debug.Log(www.error);
        }
        
    }
   void EnableLoad()
    {
        /*
        IFExistOBJ = GameObject.Find("ARObjects/" + Objnames + "(Clone)");
        
        if (IFExistOBJ == null)
        {
            StartCoroutine(loadAsset());
        }
        else
        {
            Debug.Log("Already Exist");
        }*/

        if (listcount + 2 < obj.Length)
        {
            listcount += 2;
            do
            {
                if (obj[loadpointer] != null)
                {

                    GlassesTest = Instantiate(obj[loadpointer]);
                    GlassesTest.transform.parent = GameObject.Find("GlassesNotUse").transform;//放到superGameObject物件內
                    int ChildCount = GlassesTest.transform.childCount;
                    GlassesTest.layer = 5;//層級設成UI                     


                    if (GlassesTest.transform.childCount != 0)
                    {
                        GameObject GlassT = new GameObject();

                        GlassT = GlassesTest.transform.Find(obj[loadpointer].name).gameObject;

                        if (GlassesTest.transform.Find("Glasses") != null)
                        {
                            GlassT = GlassesTest.transform.Find("Glasses").gameObject;
                            GlassT.AddComponent<GlassesAlpha>();
                        }
                    }

                    GlassesTest.AddComponent<ChildUI>();
                    GlassesTest.AddComponent<TouchDetect>();
                    GlassesTest.SetActive(false);
                }
                else
                {
                    Debug.Log("No Glasses!");
                }
                loadpointer++;
            } while (loadpointer < listcount);
        }
        else
        {
            listcount = obj.Length;
            do
            {
                if (obj[loadpointer] != null)
                {

                    GlassesTest = Instantiate(obj[loadpointer]);
                    GlassesTest.transform.parent = GameObject.Find("GlassesNotUse").transform;//放到superGameObject物件內
                    int ChildCount = GlassesTest.transform.childCount;
                    GlassesTest.layer = 5;//層級設成UI                     


                    if (GlassesTest.transform.childCount != 0)
                    {
                        GameObject GlassT = new GameObject();

                        GlassT = GlassesTest.transform.Find(obj[loadpointer].name).gameObject;

                        if (GlassesTest.transform.Find("Glasses") != null)
                        {
                            GlassT = GlassesTest.transform.Find("Glasses").gameObject;
                            GlassT.AddComponent<GlassesAlpha>();
                        }
                    }

                    GlassesTest.AddComponent<ChildUI>();
                    GlassesTest.AddComponent<TouchDetect>();
                    GlassesTest.SetActive(false);
                }
                else
                {
                    Debug.Log("No Glasses!");
                }
                loadpointer++;
            } while (loadpointer < listcount);
        }
    }
    public void picglasses()
    {
        target = GameObject.Find("Canvas/Loading Test/Label");
        Text testing = target.GetComponent<Text>();
        Objnames = testing.text;
        GameObject tmpGlasses=GameObject.Find("GlassesNotUse").transform.Find(Objnames + "(Clone)").gameObject;
        GameObject[] tmpchild=new GameObject[tmpGlasses.transform.childCount];
        tmpGlasses.SetActive(true);
        tmpGlasses.layer = 5;
        tmpGlasses.transform.parent = GameObject.Find("ARObjects").transform;
        tmpGlasses.transform.position = GameObject.Find("ARObjects").transform.position;
        if (tmpGlasses.transform.childCount == 0)
        {
            tmpGlasses.transform.localPosition = new Vector3(0, 85, -76);
        }
        else
        {

            tmpGlasses.transform.localPosition = new Vector3(0, 3, 22);
            tmpGlasses.transform.localRotation = Quaternion.Euler(-6f, 180, 0);
            if (tmpGlasses.transform.Find("Glasses") != null)
            {
                tmpchild[0] = tmpGlasses.transform.Find("Glasses").gameObject;
                tmpchild[1] = tmpGlasses.transform.Find(Objnames).gameObject;
                for (int i = 0; i < 2; i++)
                {
                    tmpchild[i].layer = 5;
                }
            }
            /*else
            {
                tmpchild[0] = transform.Find(Objnames).gameObject;
                tmpchild[0].layer = 5;
            }*/
        }
    }
    
}
