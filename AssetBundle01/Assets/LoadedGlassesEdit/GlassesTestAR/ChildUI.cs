﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ChildUI : MonoBehaviour {
    Transform[] Children, exceptTran;
    int ChildNum, exceptNum;
    
    GameObject[] Child, exceptChild, exeptSP;
    GameObject Dropdown, exceptOBJ;

    string Currentname;
    // Use this for initialization
    void Start () {
        Children = GetComponentsInChildren<Transform>();
        ChildNum = transform.childCount;

        

        if (GameObject.Find(gameObject.name + "/RayBanz-Ha") != null)
        {
            exceptOBJ = GameObject.Find(gameObject.name + "/RayBanz-Ha");
        }
        

        Child = new GameObject[ChildNum];


        for(int i=0; i < ChildNum; i++)
        {
            Child[i] = GameObject.Find(Children[i].name);
            Child[i].layer = 5;
            

        }
        if (GameObject.Find(gameObject.name + "/RayBanz-Ha") != null)
        {
            exceptTran = exceptOBJ.GetComponentsInChildren<Transform>();
            exceptNum = exceptOBJ.transform.childCount;
            exceptChild = new GameObject[exceptNum+1];
            for(int i = 1; i <= exceptNum; i++)
            {
                
                exceptChild[i] = GameObject.Find(gameObject.name + "/" + exceptOBJ.name + "/" + exceptTran[i].name);
                
                exceptChild[i].layer = 5;
            }

        }
        else if (GameObject.Find(gameObject.name + "/RayBanz-Ha") == null)
        {
            exeptSP = new GameObject[ChildNum+1];
            for (int i = 0; i <= ChildNum; i++)
            {
                exeptSP[i] = GameObject.Find(Children[i].name);
                exeptSP[i].layer = 5;

            }
        }

    }
	
	// Update is called once per frame
	void Update () {
        Dropdown = GameObject.Find("Canvas/Loading Test/Label");
        Text testing = Dropdown.GetComponent<Text>();
        Currentname = testing.text;
        
        if (gameObject.name!= Currentname + "(Clone)")
        {
            gameObject.layer=0;
            for (int i = 0; i < ChildNum; i++)
            {
                Child[i] = GameObject.Find(Children[i].name);
                Child[i].layer = 0;
            }
            if (GameObject.Find(gameObject.name + "/RayBanz-Ha") != null)
            {
                
                for (int i = 1; i <= exceptNum; i++)
                {
                    
                    exceptChild[i] = GameObject.Find(gameObject.name + "/" + exceptOBJ.name + "/" + exceptTran[i].name);                    
                    exceptChild[i].layer = 0;
                }

            }
            else if (GameObject.Find(gameObject.name + "/RayBanz-Ha") == null)
            {
                
                for (int i = 0; i <= ChildNum; i++)
                {
                    exeptSP[i] = GameObject.Find(Children[i].name);
                    exeptSP[i].layer = 0;

                }
            }
        }
        else
        {
            gameObject.layer = 5;
            for (int i = 0; i < ChildNum; i++)
            {
                Child[i] = GameObject.Find(Children[i].name);
                Child[i].layer = 5;
            }
            if (GameObject.Find(gameObject.name + "/RayBanz-Ha") != null)
            {

                for (int i = 1; i <= exceptNum; i++)
                {
                    
                    exceptChild[i] = GameObject.Find(gameObject.name + "/" + exceptOBJ.name + "/" + exceptTran[i].name);
                    
                    exceptChild[i].layer = 5;
                }

            }
            else if (GameObject.Find(gameObject.name + "/RayBanz-Ha") == null)
            {
                
                for (int i = 0; i <= ChildNum; i++)
                {
                    exeptSP[i] = GameObject.Find(Children[i].name);
                    exeptSP[i].layer = 5;

                }
            }
        }     
        


    }
}
