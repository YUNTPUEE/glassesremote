﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

// : MonoBehaviour 

public class AssetBundleBuilder
{

    // Use this for initialization
    //void Start () {

    //}

    // Update is called once per frame
    //void Update () {

    //}
    [MenuItem("Assets/Build AssetBundles/Android")]
    static void BuildAllAssetBundles()
    {
        Caching.CleanCache();

        AssetBundleBuild[] mapbuild = new AssetBundleBuild[1];
        mapbuild[0].assetBundleName = "testing";
        string[] assets = new string[4];
        assets[0] = "Assets/3DObject/20170407testing/GlassesBundle01.FBX";
        assets[1] = "Assets/3DObject/20170407testing/GlassesBundle02.FBX";
        assets[2] = "Assets/3DObject/20170407testing/GlassesBundle03.FBX";
        assets[3] = "Assets/3DObject/20170407testing/GlassesBundle04.FBX";//副檔名"一定要加"
        mapbuild[0].assetNames = assets;
        
        BuildPipeline.BuildAssetBundles("Assets/3DObject/20170407testing", mapbuild, BuildAssetBundleOptions.None, BuildTarget.Android);
    }
}
