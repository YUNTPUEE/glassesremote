﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadScript : MonoBehaviour {
    /*public bool caches = false;
    private string address = "https://172.16.31.193/ABsUpload/AssetBundleTest";

    // Use this for initialization
    private void OnGUI()
    {
        if (GUILayout.Button("Load"))
        {
            StartCoroutine(Load(address));
        }
    }

    IEnumerator Load(string url)
    {
        Debug.Log(url);
        WWW www = null;
        if (!caches)
        {
            //一般下載
            www = new WWW(url);

        }
        else
        {
            //下載至快取
            www = WWW.LoadFromCacheOrDownload(url, 0);
        }
        yield return www;
        GameObject prefab = www.assetBundle.LoadAsset("testing01.forunity") as GameObject;
        GameObject obj = GameObject.Instantiate(prefab);
        www.Dispose();
    }*/
    Vector3 OriginalLation = new Vector3(0, (float)-12, 0);
    Vector3 OriginalScale = new Vector3(110, 110, 110);
    public GameObject superGameObject;//要被放置在哪個物件底下
    public Button LoadB;
    GameObject target;
    string Objnames;
    GameObject GlassesTest, IFExistOBJ;
    GameObject[] GlassesChild;
    void Start () {
        LoadB.onClick.AddListener(EnableLoad);
    }
    // Update is called once per frame
    
    void Update () {
              


    }
    
    IEnumerator loadAsset()
    {
        
        //WWW www = new WWW("file://" + Application.dataPath + "/AssetBundleTest/AssetBundleTest");
        WWW www = new WWW("http://128.199.214.9/20170407testing/20170407testing");
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Load Success!");
            AssetBundleManifest _ABM = www.assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            if (_ABM != null)
            {
                string[] _Assetnames = _ABM.GetAllAssetBundles();
                for (int nIndex = 0; nIndex < _Assetnames.Length; ++nIndex)
                {
                    Debug.Log("Get Asset Bundle Names List" + _Assetnames[nIndex]);
                }
                //WWW ww = new WWW("file://" + Application.dataPath + "/AssetBundleTest/" + _Assetnames[0]);
                WWW ww = new WWW("http://128.199.214.9/20170407testing/" + _Assetnames[0]);
                yield return ww;
                if (string.IsNullOrEmpty(ww.error))
                {
                    GameObject obj = ww.assetBundle.LoadAsset<GameObject>(Objnames);
                    if (obj != null)
                    {
                        GlassesTest = Instantiate(obj);
                        GlassesTest.transform.parent = superGameObject.transform;//放到superGameObject物件內
                        int ChildCount = GlassesTest.transform.childCount;
                        GlassesTest.layer = 5;//層級設成UI                        
                        //===============================================================================
                        GlassesTest.transform.localPosition = OriginalLation;
                        GlassesTest.transform.localScale = OriginalScale;
                        GlassesTest.transform.localRotation = Quaternion.Euler(-5, 180, 0);
                        //======================更改載入物件的基本屬性=========================================
                        GlassesTest.AddComponent<ChildUI>();


                    }
                    else
                    {
                        Debug.Log("No Glasses!");
                    }

                    ww.assetBundle.Unload(false);
                }
                else
                {
                    Debug.Log("Asset" + _Assetnames[0] + "is not exist!");
                }
            }
            else
            {
                Debug.Log("No Manifest");
            }

            www.assetBundle.Unload(false);
        }
        else
        {
            Debug.Log(www.error);
        }
        
    }
   void EnableLoad()
    {
        target = GameObject.Find("Canvas/Loading Test/Label");
        Text testing = target.GetComponent<Text>();
        Objnames = testing.text;
        IFExistOBJ = GameObject.Find("ARObjects/" + Objnames + "(Clone)");
        
        if (IFExistOBJ == null)
        {
            StartCoroutine(loadAsset());
        }
        else
        {
            Debug.Log("Already Exist");
        }
        
    }
}
