﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif

public class ONUI : MonoBehaviour {


    public void ClickedS()
    {
        SceneManager.LoadScene("WebCamTextureSample");
    }

    public void Clicked()
    {
        Debug.Log("玩家按下UI按鈕");
    }


    public void Quit()
    {
        Application.Quit();
    }

    public void ARText()
    {
        SceneManager.LoadScene("WebCamTextureARSample");
    }
}
