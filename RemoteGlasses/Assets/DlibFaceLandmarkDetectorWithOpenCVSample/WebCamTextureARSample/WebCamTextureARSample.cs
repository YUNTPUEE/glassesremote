﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;
using DlibFaceLandmarkDetector;

namespace DlibFaceLandmarkDetectorSample
{
    /// <summary>
    /// Face tracker AR from WebCamTexture Sample.
    /// This sample was referring to http://www.morethantechnical.com/2012/10/17/head-pose-estimation-with-opencv-opengl-revisited-w-code/
    /// and use effect asset from http://ktk-kumamoto.hatenablog.com/entry/2014/09/14/092400
    /// </summary>
    [RequireComponent(typeof(WebCamTextureToMatHelper))]
    public class WebCamTextureARSample : MonoBehaviour
    {
        /// <summary>
        /// The is showing face points.
        /// </summary>
        public bool isShowingFacePoints;

        /// <summary>
        /// The is showing face points toggle.
        /// </summary>
        public Toggle isShowingFacePointsToggle;

        /// <summary>
        /// The is showing axes.
        /// </summary>
        public bool isShowingAxes;

        /// <summary>
        /// The is showing axes toggle.
        /// </summary>
        public Toggle isShowingAxesToggle;

        /// <summary>
        /// The is showing head.
        /// </summary>
        public bool isShowingHead;

        /// <summary>
        /// The is showing head toggle.
        /// </summary>
        public Toggle isShowingHeadToggle;

        /// <summary>
        /// The is showing effects.
        /// </summary>
        public bool isShowingEffects;

        /// <summary>
        /// The is showing effects toggle.
        /// </summary>
        public Toggle isShowingEffectsToggle;

        //========================顯示眼鏡宣告====================================
        public bool isShowingGlasses;
        public Toggle isShowingGlassesToggle;
        public GameObject Glasses;

        public bool isShowingGlasses_002;
        public Toggle isShowingGlasses_002Toggle;
        public GameObject Glasses_002;
        //=======================================================================

        /// <summary>
        /// The axes.
        /// </summary>
        public GameObject axes;
        
        /// <summary>
        /// The head.
        /// </summary>
        public GameObject head;
        
        /// <summary>
        /// The right eye.
        /// </summary>
        public GameObject rightEye;
        
        /// <summary>
        /// The left eye.
        /// </summary>
        public GameObject leftEye;
        
        /// <summary>
        /// The mouth.
        /// </summary>
        public GameObject mouth;

        /// <summary>
        /// The mouth particle system.
        /// </summary>
        ParticleSystem[] mouthParticleSystem;
        
        /// <summary>
        /// The texture.
        /// </summary>
        Texture2D texture;

        /// <summary>
        /// The face landmark detector.
        /// </summary>
        FaceLandmarkDetector faceLandmarkDetector;
        
        /// <summary>
        /// The AR camera.
        /// </summary>
        public Camera ARCamera;
        
        /// <summary>
        /// The cam matrix.
        /// </summary>
        Mat camMatrix;
        
        /// <summary>
        /// The dist coeffs.
        /// </summary>
        MatOfDouble distCoeffs;
        
        /// <summary>
        /// The invert Y.
        /// </summary>
        Matrix4x4 invertYM;
        
        /// <summary>
        /// The transformation m.
        /// </summary>
        Matrix4x4 transformationM = new Matrix4x4 ();
        
        /// <summary>
        /// The invert Z.
        /// </summary>
        Matrix4x4 invertZM;
        
        /// <summary>
        /// The ar m.
        /// </summary>
        Matrix4x4 ARM;

        /// <summary>
        /// The ar game object.
        /// </summary>
        public GameObject ARGameObject;

        /// <summary>
        /// The should move AR camera.
        /// </summary>
        public bool shouldMoveARCamera;
        
        /// <summary>
        /// The 3d face object points.
        /// </summary>
        MatOfPoint3f objectPoints;
        
        /// <summary>
        /// The image points.
        /// </summary>
        MatOfPoint2f imagePoints;
        
        /// <summary>
        /// The rvec.
        /// </summary>
        Mat rvec;
        
        /// <summary>
        /// The tvec.
        /// </summary>
        Mat tvec;
        
        /// <summary>
        /// The rot m.
        /// </summary>
        Mat rotM;

        /// <summary>
        /// The web cam texture to mat helper.
        /// </summary>
        WebCamTextureToMatHelper webCamTextureToMatHelper;
        //KalmanFilter====================================
        //KalmanFilter KF;
        
        int[][] I6AH =
        {
            new [] { 1, 0, 0, 0, 0, 0 },
            new [] { 0, 1, 0, 0, 0, 0 },
            new [] { 0, 0, 1, 0, 0, 0 },
            new [] { 0, 0, 0, 1, 0, 0 },
            new [] { 0, 0, 0, 0, 1, 0 },
            new [] { 0, 0, 0, 0, 0, 1 }
        };
        public float[][] StateMatrix = 
        {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };
        public float[][] PNC =
            {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };//Process Noise Covariance , Q
        public float[][] MNC =
            {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };//Measurement Noise Covariance , R
        public float[][] MeasureMatrix =
            {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };//Measurement , z0head
        public float[][] Estimate =
            {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };
        public float[][] EstimateI =
            {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };//Estimate = 最終輸出 , x1。 加Ｉ表示反矩陣
        public float[][] PEC =
            {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };
        public float[][] PECI =
            {
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
            new float[] {0},
        };//Posteriori Error Covariance , Pk-1 。 加Ｉ表示反矩陣 , Priori Error Covariance, PkI
        float Gain =0;
        float[] GainIM = new float[6] {0, 0, 0, 0, 0, 0};
        //Mat transmitionMatrix, MeasurmentMatrix, StatePreM, errorCov, measurmentError, processNoiseCov, Prediction, estimated;
        //Mat testing;
        //==================================================
        /// <summary>
        /// The shape_predictor_68_face_landmarks_dat_filepath.
        /// </summary>
        private string shape_predictor_68_face_landmarks_dat_filepath;

        // Use this for initialization
        void Start ()
        {
            isShowingFacePointsToggle.isOn = isShowingFacePoints;
            isShowingAxesToggle.isOn = isShowingAxes;
            isShowingHeadToggle.isOn = isShowingHead;
            isShowingEffectsToggle.isOn = isShowingEffects;
            //===============================眼鏡初期化==========================
            isShowingGlassesToggle.isOn = isShowingGlasses;
            isShowingGlasses_002Toggle.isOn = isShowingGlasses_002;
            //==================================================================
            /*KF = new KalmanFilter();
            transmitionMatrix = new Mat(6, 6, CvType.CV_64FC1);
            MeasurmentMatrix = new Mat(6, 6, CvType.CV_64FC1);
            processNoiseCov = new Mat(6, 1, CvType.CV_64FC1, new Scalar(1e-4));
            measurmentError = new Mat(6, 1, CvType.CV_64FC1, new Scalar(10));
            errorCov = new Mat(6, 6, CvType.CV_64FC1, new Scalar(0));
            StatePreM = new Mat(6, 1, CvType.CV_64FC1);
            Prediction = new Mat(6, 1, CvType.CV_64FC1);
            testing = new Mat(6, 1, CvType.CV_64FC1);
            

            
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                    MeasurmentMatrix.put(i, j, matrix[i][j]);
            }
            */
            //以上是宣告-------------------------------------------------------
            /*KF.set_transitionMatrix(transmitionMatrix);
            KF.set_statePre(StatePreM);
            KF.set_measurementMatrix(MeasurmentMatrix);
            KF.set_processNoiseCov(processNoiseCov);
            KF.set_measurementNoiseCov(measurmentError);
            KF.set_errorCovPost(errorCov);*/
            int noise = 10;

            StateMatrix[0][0] = ARGameObject.transform.position.x;
            StateMatrix[1][0] = ARGameObject.transform.position.y;
            StateMatrix[2][0] = ARGameObject.transform.position.z;
            StateMatrix[3][0] = ARGameObject.transform.rotation.x;
            StateMatrix[4][0] = ARGameObject.transform.rotation.y;
            StateMatrix[5][0] = ARGameObject.transform.rotation.z; // x0head
            for (int i = 0; i < 6; i++)
            {
                PNC[i][0] = noise;
                MNC[i][0] = noise;
                MeasureMatrix[i][0] = 0;
                Estimate[i][0] = 0;
                EstimateI[i][0] = 0;
                PEC[i][0] = 0;
                PECI[i][0] = 0;
            }                   
            //KalmanFilter=============================

#if UNITY_WEBGL && !UNITY_EDITOR
            StartCoroutine(DlibFaceLandmarkDetector.Utils.getFilePathAsync("shape_predictor_68_face_landmarks.dat", (result) => {
                shape_predictor_68_face_landmarks_dat_filepath = result;
                Run ();
            }));
#else
            shape_predictor_68_face_landmarks_dat_filepath = DlibFaceLandmarkDetector.Utils.getFilePath ("shape_predictor_68_face_landmarks.dat");
            Run ();
            #endif
        }
        
        private void Run ()
        {
            //set 3d face object points.
            objectPoints = new MatOfPoint3f (
                //new Point3 (-31, 72, 86),//l eye
                //new Point3 (31, 72, 86),//r eye
                new Point3(40, 68, 73),//外側眼角(左)
                new Point3(-40, 68, 73),//外側眼角(右)
                new Point3(18, 68, 78),//內側眼角(左)
                new Point3(-18, 68, 78),//內側眼角(右)
                new Point3 (0, 40, 114),//nose
                new Point3(0, 72, 90),//眉心
                new Point3(11, 34, 88),//左鼻翼
                new Point3(-11, 34, 88),//右鼻翼
                new Point3 (-20, 15, 90),//l mouse
                new Point3 (20, 15, 90),//r mouse
                //new Point3(-39, 6, 68),//l mouseD
                //new Point3(39, 6, 68),//r mouseD
                new Point3(22, -22, 71),//下巴左側
                new Point3(-22, -22, 71),//下巴右側
                new Point3(0, -30, 78)//下巴
                //new Point3 (-69, 76, -2),//l ear
                //new Point3 (69, 76, -2)//r ear

            );
            imagePoints = new MatOfPoint2f ();
            rvec = new Mat ();
            tvec = new Mat ();
            rotM = new Mat (3, 3, CvType.CV_64FC1);

            faceLandmarkDetector = new FaceLandmarkDetector (shape_predictor_68_face_landmarks_dat_filepath);

            webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper> ();
            webCamTextureToMatHelper.Init ();
        }

        /// <summary>
        /// Raises the web cam texture to mat helper inited event.
        /// </summary>
        public void OnWebCamTextureToMatHelperInited ()
        {
            Debug.Log ("OnWebCamTextureToMatHelperInited");
            
            Mat webCamTextureMat = webCamTextureToMatHelper.GetMat ();
            
            texture = new Texture2D (webCamTextureMat.cols (), webCamTextureMat.rows (), TextureFormat.RGBA32, false);

            gameObject.GetComponent<Renderer> ().material.mainTexture = texture;

            gameObject.transform.localScale = new Vector3 (webCamTextureMat.cols (), webCamTextureMat.rows (), 1);
            Debug.Log ("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);
            

            float width = webCamTextureMat.width ();
            float height = webCamTextureMat.height ();
            
            float imageSizeScale = 1.0f;
            float widthScale = (float)Screen.width / width;
            float heightScale = (float)Screen.height / height;
            if (widthScale < heightScale) {
                Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
                imageSizeScale = (float)Screen.height / (float)Screen.width;
            } else {
                Camera.main.orthographicSize = height / 2;
            }
            
            
            //set cameraparam
            int max_d = (int)Mathf.Max (width, height);
            double fx = max_d;
            double fy = max_d;
            double cx = width / 2.0f;
            double cy = height / 2.0f;
            camMatrix = new Mat (3, 3, CvType.CV_64FC1);
            camMatrix.put (0, 0, fx);
            camMatrix.put (0, 1, 0);
            camMatrix.put (0, 2, cx);
            camMatrix.put (1, 0, 0);
            camMatrix.put (1, 1, fy);
            camMatrix.put (1, 2, cy);
            camMatrix.put (2, 0, 0);
            camMatrix.put (2, 1, 0);
            camMatrix.put (2, 2, 1.0f);
            Debug.Log ("camMatrix " + camMatrix.dump ());
            
            
            distCoeffs = new MatOfDouble (0, 0, 0, 0);
            Debug.Log ("distCoeffs " + distCoeffs.dump ());
            
            
            //calibration camera
            Size imageSize = new Size (width * imageSizeScale, height * imageSizeScale);
            double apertureWidth = 0;
            double apertureHeight = 0;
            double[] fovx = new double[1];
            double[] fovy = new double[1];
            double[] focalLength = new double[1];
            Point principalPoint = new Point (0, 0);
            double[] aspectratio = new double[1];
            
            Calib3d.calibrationMatrixValues (camMatrix, imageSize, apertureWidth, apertureHeight, fovx, fovy, focalLength, principalPoint, aspectratio);
            
            Debug.Log ("imageSize " + imageSize.ToString ());
            Debug.Log ("apertureWidth " + apertureWidth);
            Debug.Log ("apertureHeight " + apertureHeight);
            Debug.Log ("fovx " + fovx [0]);
            Debug.Log ("fovy " + fovy [0]);
            Debug.Log ("focalLength " + focalLength [0]);
            Debug.Log ("principalPoint " + principalPoint.ToString ());
            Debug.Log ("aspectratio " + aspectratio [0]);
            
            
            //To convert the difference of the FOV value of the OpenCV and Unity. 
            double fovXScale = (2.0 * Mathf.Atan ((float)(imageSize.width / (2.0 * fx)))) / (Mathf.Atan2 ((float)cx, (float)fx) + Mathf.Atan2 ((float)(imageSize.width - cx), (float)fx));
            double fovYScale = (2.0 * Mathf.Atan ((float)(imageSize.height / (2.0 * fy)))) / (Mathf.Atan2 ((float)cy, (float)fy) + Mathf.Atan2 ((float)(imageSize.height - cy), (float)fy));
            
            Debug.Log ("fovXScale " + fovXScale);
            Debug.Log ("fovYScale " + fovYScale);
            
            
            //Adjust Unity Camera FOV https://github.com/opencv/opencv/commit/8ed1945ccd52501f5ab22bdec6aa1f91f1e2cfd4
            if (widthScale < heightScale) {
                ARCamera.fieldOfView = (float)(fovx [0] * fovXScale);
            } else {
                ARCamera.fieldOfView = (float)(fovy [0] * fovYScale);
            }
                                    
                                    
                                    
            invertYM = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, -1, 1));
            Debug.Log ("invertYM " + invertYM.ToString ());
            
            invertZM = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, 1, -1));
            Debug.Log ("invertZM " + invertZM.ToString ());
            
            
            axes.SetActive (false);
            head.SetActive (false);
            rightEye.SetActive (false);
            leftEye.SetActive (false);
            mouth.SetActive (false);
            Glasses.SetActive(false);
            Glasses_002.SetActive(false);

            mouthParticleSystem = mouth.GetComponentsInChildren<ParticleSystem> (true);

        }
        
        /// <summary>
        /// Raises the web cam texture to mat helper disposed event.
        /// </summary>
        public void OnWebCamTextureToMatHelperDisposed ()
        {
            Debug.Log ("OnWebCamTextureToMatHelperDisposed");

            camMatrix.Dispose ();
            distCoeffs.Dispose ();
        }

        // Update is called once per frame
        void Update ()
        {

            if (webCamTextureToMatHelper.isPlaying () && webCamTextureToMatHelper.didUpdateThisFrame ()) {
                
                Mat rgbaMat = webCamTextureToMatHelper.GetMat ();
                
                
                OpenCVForUnityUtils.SetImage (faceLandmarkDetector, rgbaMat);

                //detect face rects
                List<UnityEngine.Rect> detectResult = faceLandmarkDetector.Detect ();

                if (detectResult.Count > 0) {

                    //detect landmark points
                    List<Vector2> points = faceLandmarkDetector.DetectLandmark (detectResult [0]);

                    if (points.Count > 0) {
                        if (isShowingFacePoints)
                            OpenCVForUnityUtils.DrawFaceLandmark (rgbaMat, points, new Scalar (0, 255, 0, 255), 2);                    

                        imagePoints.fromArray (
                            //new Point ((points [36].x + points [39].x) / 2, (points [36].y + points [39].y) / 2),//l eye
                            //new Point ((points [42].x + points [45].x) / 2, (points [42].y + points [45].y) / 2),//r eye
                            new Point(points[45].x, points[45].y),//外側眼角(左)
                            new Point(points[36].x, points[36].y),//外側眼角(右)
                            new Point(points[42].x, points[42].y),//內側眼角(左)
                            new Point(points[39].x, points[39].y),//內側眼角(右)                            
                            new Point (points [33].x, points [33].y),//nose
                            new Point(points[27].x, points[27].y),//眉心
                            new Point(points[31].x, points[31].y),//右鼻翼
                            new Point(points[35].x, points[35].y),//左鼻翼
                            new Point (points [48].x, points [48].y),//l mouth
                            new Point (points [54].x, points [54].y), //r mouth
                            //new Point(points[4].x, points[4].y), //r mouthD
                            //new Point(points[12].x, points[12].y), //l mouthD
                            new Point(points[9].x, points[9].y), //下巴左側
                            new Point(points[7].x, points[7].y), //下巴右側
                            new Point(points[8].x, points[8].y) //下巴
                                                                  //new Point (points [0].x, points [0].y),//l ear
                                                                  //new Point (points [16].x, points [16].y)//r ear
                        );
                                                                        
                                                                        
                        Calib3d.solvePnP (objectPoints, imagePoints, camMatrix, distCoeffs, rvec, tvec);


                        if (tvec.get (2, 0) [0] > 0) {

                            if (Mathf.Abs ((float)(points [43].y - points [46].y)) > Mathf.Abs ((float)(points [42].x - points [45].x)) / 6.0) {
                                if (isShowingEffects)
                                    rightEye.SetActive (true);
                            }

                            if (Mathf.Abs ((float)(points [38].y - points [41].y)) > Mathf.Abs ((float)(points [39].x - points [36].x)) / 6.0) {
                                if (isShowingEffects)
                                    leftEye.SetActive (true);
                            }
                            if (isShowingHead)
                                head.SetActive (true);
                            if (isShowingAxes)
                                axes.SetActive (true);
                            //===================================眼鏡顯示=====================
                            if (isShowingGlasses)
                                Glasses.SetActive(true);

                            if (isShowingGlasses_002)
                                Glasses_002.SetActive(true);
                            //================================================================                        

                            float noseDistance = Mathf.Abs ((float)(points [27].y - points [33].y));
                            float mouseDistance = Mathf.Abs ((float)(points [62].y - points [66].y));
                            if (mouseDistance > noseDistance / 5.0) {
                                if (isShowingEffects) {
                                    mouth.SetActive (true);
                                    foreach (ParticleSystem ps in mouthParticleSystem) {
                                        ps.enableEmission = true;
                                        ps.startSize = 40 * (mouseDistance / noseDistance);
                                    }
                                }
                            } else {
                                if (isShowingEffects) {
                                    foreach (ParticleSystem ps in mouthParticleSystem) {
                                        ps.enableEmission = false;
                                    }
                                }
                            }
                                                       
                            Calib3d.Rodrigues (rvec, rotM); //void Rodrigues(InputArray src, OutputArray dst, OutputArray jacobian=noArray())

                            

                            transformationM .SetRow (0, new Vector4 ((float)rotM.get (0, 0) [0], (float)rotM.get (0, 1) [0], (float)rotM.get (0, 2) [0], (float)tvec.get (0, 0) [0]));
                            transformationM.SetRow (1, new Vector4 ((float)rotM.get (1, 0) [0], (float)rotM.get (1, 1) [0], (float)rotM.get (1, 2) [0], (float)tvec.get (1, 0) [0]));
                            transformationM.SetRow (2, new Vector4 ((float)rotM.get (2, 0) [0], (float)rotM.get (2, 1) [0], (float)rotM.get (2, 2) [0], (float)tvec.get (2, 0) [0]));
                            transformationM.SetRow (3, new Vector4 (0, 0, 0, 1));

                            //KalmanFilter.ReferenceEquals(objectPoints, transformationM);
                            //KalmanFilter.Equals(objectPoints, transformationM);


                            if (shouldMoveARCamera) {

                                if (ARGameObject != null) {
                                    ARM = ARGameObject.transform.localToWorldMatrix * invertZM * transformationM.inverse * invertYM;
                                    
                                    ARUtils.SetTransformFromMatrix (ARCamera.transform, ref ARM);                                    
                                    //ARGameObject.SetActive (true);
                                }
                            } else {
                                ARM = ARCamera.transform.localToWorldMatrix * invertYM * transformationM * invertZM;
                                
                                if (ARGameObject != null) {
                                    ARUtils.SetTransformFromMatrix (ARGameObject.transform, ref ARM);                                    
                                    //ARGameObject.SetActive (true);
                                }
                            }

                        }
                    }
                }
                
                Imgproc.putText (rgbaMat, "W:" + rgbaMat.width () + " H:" + rgbaMat.height () + " SO:" + Screen.orientation, new Point (5, rgbaMat.rows () - 10), Core.FONT_HERSHEY_SIMPLEX, 0.5, new Scalar (255, 255, 255, 255), 1, Imgproc.LINE_AA, false);
                                        
                OpenCVForUnity.Utils.matToTexture2D (rgbaMat, texture, webCamTextureToMatHelper.GetBufferColors ());
                                        
            }
            //作卡爾曼濾波==============================================
            /*KF.predict();

            testing.put(0, 0, ARGameObject.transform.position.x);
            testing.put(1, 0, ARGameObject.transform.position.y);
            testing.put(2, 0, ARGameObject.transform.position.z);
            testing.put(3, 0, ARGameObject.transform.rotation.x);
            testing.put(4, 0, ARGameObject.transform.rotation.y);
            testing.put(5, 0, ARGameObject.transform.rotation.z);
                        
            estimated = KF.correct(testing);*/
            MeasureMatrix[0][0] = ARGameObject.transform.position.x;
            MeasureMatrix[1][0] = ARGameObject.transform.position.y;
            MeasureMatrix[2][0] = ARGameObject.transform.position.z;
            MeasureMatrix[3][0] = ARGameObject.transform.rotation.x;
            MeasureMatrix[4][0] = ARGameObject.transform.rotation.y;
            MeasureMatrix[5][0] = ARGameObject.transform.rotation.z;

            for (int i =0; i < 6; i++)
            {
                EstimateI[i][0] = StateMatrix[i][0] * (float)I6AH[i][i];
                PECI[i][0] = PEC[i][0] * (float)I6AH[i][i] + PNC[i][0];
            }//xkheadI=A*xk-1head , PkI=A*Pk-1*AT+Q
            for(int i=0; i<6; i++)
            {
                GainIM[i] = PECI[i][0] * (float)I6AH[i][i] + MNC[i][0];
            }
            for (int i = 0; i < 6; i++)
            {
                Gain = Gain+PECI[i][0] * (float)I6AH[i][i]* GainIM[i];
            }//Kk=PkI*HT*(H*PkI*HT+R)I
            for (int i = 0; i < 6; i++)
            {
                Estimate[i][0] = EstimateI[i][0] + Gain * (MeasureMatrix[i][0] - EstimateI[i][0] * (float)I6AH[i][i]);
            }//xkhead=xkheadI+Kk*(Zk-H*xheadI)
            for (int i = 0; i < 6; i++)
            {
                PEC[i][0] = ((float)I6AH[i][i] - Gain * (float)I6AH[i][i]) * PECI[i][0];
            }//Pk=(I-Kk*H)*PkI
            
            ARGameObject.transform.position.Set(Estimate[0][0], Estimate[1][0], Estimate[2][0]);
            ARGameObject.transform.rotation.Set(Estimate[3][0], Estimate[4][0], Estimate[5][0], ARGameObject.transform.rotation.w);
            ARGameObject.SetActive(true);
            //更新一些矩陣=========================
            for (int i = 0; i < 6; i++)
            {
                StateMatrix[i][0] = Estimate[i][0];
            }

            //======================================================       
        }

        /// <summary>
        /// Raises the disable event.
        /// </summary>
        void OnDisable ()
        {
            if (webCamTextureToMatHelper != null)
                webCamTextureToMatHelper.Dispose ();

            if (faceLandmarkDetector != null)
                faceLandmarkDetector.Dispose ();
        }
        
        /// <summary>
        /// Raises the back button event.
        /// </summary>
        public void OnBackButton ()
        {
#if UNITY_5_3 || UNITY_5_3_OR_NEWER
            //SceneManager.LoadScene ("DlibFaceLandmarkDetectorSample");
            SceneManager.LoadScene("facedetection");
#else
            Application.LoadLevel ("DlibFaceLandmarkDetectorSample");
#endif
        }
        
        /// <summary>
        /// Raises the play button event.
        /// </summary>
        public void OnPlayButton ()
        {
            webCamTextureToMatHelper.Play ();
        }
        
        /// <summary>
        /// Raises the pause button event.
        /// </summary>
        public void OnPauseButton ()
        {
            webCamTextureToMatHelper.Pause ();
        }
        
        /// <summary>
        /// Raises the stop button event.
        /// </summary>
        public void OnStopButton ()
        {
            webCamTextureToMatHelper.Stop ();
        }
        
        /// <summary>
        /// Raises the change camera button event.
        /// </summary>
        public void OnChangeCameraButton ()
        {
            webCamTextureToMatHelper.Init (null, webCamTextureToMatHelper.requestWidth, webCamTextureToMatHelper.requestHeight, !webCamTextureToMatHelper.requestIsFrontFacing);
        }
                
        /// <summary>
        /// Raises the is showing face points toggle event.
        /// </summary>
        public void OnIsShowingFacePointsToggle ()
        {
            if (isShowingFacePointsToggle.isOn) {
                isShowingFacePoints = true;
            } else {
                isShowingFacePoints = false;
            }
        }
        
        /// <summary>
        /// Raises the is showing axes toggle event.
        /// </summary>
        public void OnIsShowingAxesToggle ()
        {
            if (isShowingAxesToggle.isOn) {
                isShowingAxes = true;
            } else {
                isShowingAxes = false;
                axes.SetActive (false);
            }
        }
        
        /// <summary>
        /// Raises the is showing head toggle event.
        /// </summary>
        public void OnIsShowingHeadToggle ()
        {
            if (isShowingHeadToggle.isOn) {
                isShowingHead = true;
            } else {
                isShowingHead = false;
                head.SetActive (false);
            }
        }
        
        /// <summary>
        /// Raises the is showin effects toggle event.
        /// </summary>
        public void OnIsShowinEffectsToggle ()
        {
            if (isShowingEffectsToggle.isOn) {
                isShowingEffects = true;
            } else {
                isShowingEffects = false;
                rightEye.SetActive (false);
                leftEye.SetActive (false);
                mouth.SetActive (false);
            }
        }

        //===================================眼鏡checkbox==================
        public void OnIsSHowingGlassesToggle ()
        {
            if (isShowingGlassesToggle.isOn)
            {
                isShowingGlasses = true;
            }
            else
            {
                isShowingGlasses = false;
                Glasses.SetActive (false);
            }
        }

        public void OnIsSHowingGlasses_002Toggle()
        {
            if (isShowingGlasses_002Toggle.isOn)
            {
                isShowingGlasses_002 = true;
            }
            else
            {
                isShowingGlasses_002 = false;
                Glasses_002.SetActive(false);
            }
        }
        //===============================================================

    }
}